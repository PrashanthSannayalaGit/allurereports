package tests;

import com.google.common.collect.ImmutableMap;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import pages.HomePage;
import pages.LoginPage;

import java.io.IOException;

import static com.github.automatedowl.tools.AllureEnvironmentWriter.allureEnvironmentWriter;

public class BaseTest {
    public WebDriver driver;
    public HomePage homePage;
    public LoginPage loginPage;

    public WebDriver getDriver() {
        return driver;
    }



    @BeforeClass
    public void classLevelSetup() {
        allureEnvironmentWriter(
                ImmutableMap.<String, String>builder()
                        .put("Browser", "Chrome")
                        .put("Browser.Version", "70.0.3538.77")
                        .put("URL", "http://abc1233234.net")
                        .build(), System.getProperty("user.dir")
                        + "/allure-results/");

        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//Drivers//Chromedriver_77");
        driver = new ChromeDriver();
    }

    @BeforeMethod
    public void methodLevelSetup() {
        homePage = new HomePage(driver);
        loginPage = new LoginPage(driver);
    }

    @AfterClass
    public void teardown() throws IOException {
        driver.quit();

    }
    @AfterSuite
    public void afterSuite() throws IOException {
        String[] abc = new String[] {"/bin/bash", "-c", "allure serve allure-results"};
        Process proc = new ProcessBuilder(abc).start();
    }
}