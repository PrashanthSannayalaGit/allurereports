package tests;

import io.qameta.allure.*;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.Listeners.TestListener;

import java.io.IOException;

//In order to eliminate attachment problem for Allure, you should add @Listener line.
//link: https://github.com/allure-framework/allure1/issues/730
@Listeners({ TestListener.class })
//@Epic("Regression Tests")
@Feature("Login Tests")
public class LoginTests extends BaseTest {

    //Test Data
    String wrongUsername = "onur@swtestacademy.com";
    String wrongPassword = "11122233444";

    @Test (priority = 0, description="Invalid Login Scenario with wrong username and password.")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Test Description: Login test with wrong username and wrong password.")
    @Story("Invalid username and password login test")
    public void invalidLoginTest_InvalidUserNameInvalidPassword () {
       /* homePage
            .goToN11("GIVEN : ")
            .goToLoginPage("WHEN :")
            .loginToN11("AND : ",wrongUsername, wrongPassword)
            .verifyLoginPassword("THEN : ","E-posta adresiniz veya şifreniz hatalı");*/
       invalidLoginTest_Given();
       invalidLoginTest_When();
       invalidLoginTest_And(wrongUsername,wrongPassword);
       invalidLoginTest_Then("E-posta adresiniz veya şifreniz hatalı");
    }

    @Step("GIVEN Open N11 Step...")
    public void invalidLoginTest_Given(){
        homePage.goToN11();

    }
    @Step("WHEN Go to Login Page Step...")
    public void invalidLoginTest_When(){
        homePage.goToLoginPage();
    }
    @Step("AND Login Step with username: {0}, password: {1}, for method: {method} step...")
    public void invalidLoginTest_And(String wrongUsername, String wrongPassword){
                loginPage.loginToN11("AND : ",wrongUsername, wrongPassword);
    }
    @Step("THEN xVerify verifyLoginPassword: {0} step...")
    public void invalidLoginTest_Then(String expectedText){
        loginPage.verifyLoginPassword(expectedText);
    }

    /*@Test (priority = 1, description="Invalid Login Scenario with empty username and password.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("Test Description: Login test with empty username and empty password.")
    @Story("Empty username and password login test")
    public void invalidLoginTest_EmptyUserEmptyPassword () {
        homePage
            .goToN11()
            .goToLoginPage()
            .loginToN11("", "")
            .verifyLoginUserName("Lütfen e-posta adresinizi girin.")
            .verifyLoginPassword("WRONG MESSAGE FOR FAILURE!");
    }*/


    public static void main(String[] args) throws IOException {
        //Runtime.getRuntime().exec("/bin/bash -c allure serve allure-results");

//        String[] cmd = {"/bin/bash","allure serve allure-results", System.getProperty("user.dir")+"//allure-results//"};
//        Runtime.getRuntime().exec(cmd);

        /*String[] abc = new String[] {"/bin/bash", "-c", "allure serve allure-results"};
        Process proc = new ProcessBuilder(abc).start();*/
    }

}