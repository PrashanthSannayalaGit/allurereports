package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPage extends BasePage {

    /**Constructor*/
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    /**Web Elements*/
    By userNameId = By.id("email");
    By passwordId = By.id("password");
    By loginButtonId = By.id("loginButton");
    By errorMessageUsernameXpath = By.xpath("//*[@id=\"loginForm\"]/div[1]/div/div");
    By errorMessagePasswordXpath = By.xpath("//*[@id=\"loginForm\"]/div[2]/div/div ");

    /**Page Methods*/
    @Step("{0} Login Step with username: {1}, password: {2}, for method: {method} step...")
    public LoginPage loginToN11(String and, String username, String password) {
        writeText(userNameId, username);
        writeText(passwordId, password);
        click(loginButtonId);
        return this;
    }

    //Verify Username Condition
    @Step("Verify username: {0} step...")
    public LoginPage verifyLoginUserName(String expectedText) {
        block1:
        {
            waitVisibility(errorMessageUsernameXpath);
            Assert.assertEquals(readText(errorMessageUsernameXpath), expectedText);
        }

        return this;
    }

    @Test
    private void test1(){

        {
            waitVisibility(errorMessageUsernameXpath);

        }

    }

    //Verify Password Condition
    //@Step("{0} Verify verifyLoginPassword: {1} step...")
    public LoginPage verifyLoginPassword( String expectedText) {
        waitVisibility(errorMessagePasswordXpath);
        Assert.assertEquals(readText(errorMessagePasswordXpath), expectedText);
        return this;
    }
}